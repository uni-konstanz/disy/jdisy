/*
	Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de
	
	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
 */
package org.slf4j.impl;

import org.slf4j.ILoggerFactory;

import de.unikn.disy.log.DisyLoggerFactory;

/**
 * @author zink
 */
public class StaticLoggerBinder {
	private static final StaticLoggerBinder SINGLETON = new StaticLoggerBinder();
	public static final String REQUESTED_API_VERSION = "1.6.4";
	private final ILoggerFactory factory = new DisyLoggerFactory();

	private StaticLoggerBinder() {}
	
	public static final StaticLoggerBinder getSingleton() {
		return SINGLETON;
	}

	public ILoggerFactory getLoggerFactory() {
		return factory;
	}

	public String getLoggerFactoryClassStr() {
		return DisyLoggerFactory.class.toString();
	}
}

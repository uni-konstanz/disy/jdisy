Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.


#	jdisy - Java general purpose library

This java library unifies classes that were created during research projects
in the Distributed Systems Lab of the University of Konstanz. It covers 
multiple different purposes.

The whole project is split into multiple maven projects. Apart from junit
for unit testing most of the projects have no notable dependencies and can
be built individually. An ant build file is sometimes provided.

##	CONTENTS

* 	de.unikn.disy.parent

	The parent project.

*	de.disy.unikn.util	

	Utility parent project.

*	de.unikn.disy.util.convert

	Utility classes for reoccurring object conversions.

* 	de.unikn.disy.util.factory

	Static factory methods to simplify instantiation, introduce some
	sort of type inference and reduce code verbosity.

*	de.unikn.disy.hash

	A java hash library.

*	de.unikn.disy.log

	Disy Logger project.

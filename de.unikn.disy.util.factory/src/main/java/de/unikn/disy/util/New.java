/*
	Copyright (c) 2010-2012 Thomas Zink

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
 */
package de.unikn.disy.util;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Static factories that provide type inference and remove redundancies in
 * instantiation. Provides factory methods for most commonly used java.util
 * data types. 
 * 
 * The downside is, that by importing this class all the supported
 * java.util classes are imported as well even if not used at all in the
 * client program. However, it might be possible to avoid this by using static
 * imports of those factory methods the client really requires. E.g.
 * 
 * 		import static de.unikn.disy.util.New.HashMap;
 * 
 * Inspired by 
 * Joshua Bloch, "Effective Java second edition", Addison-Wesley, 2008 
 * 
 * @author thomas zink
 */
public final class New {
	// HashMap
	public static <K,V> HashMap<K,V> HashMap() {
	   	return new HashMap<K,V>();
	}

    public static <K,V> HashMap<K,V> HashMap(int initialCapacity) {
    	return new HashMap<K, V>(initialCapacity);
    }
    
    public static <K,V> HashMap<K,V> HashMap(HashMap<K,V> m) {
    	return new HashMap<K, V>(m);
    }
    
    public static <K,V> HashMap<K,V> HashMap(int initialCapacity, int loadFactor) {	
    	return new HashMap<K, V>(initialCapacity, loadFactor);
    }
    
    // HashSet
    public static <E> HashSet<E> HashSet() {
    	return new HashSet<E>();
    }
    
    public static <E> HashSet<E> HashSet(int initialCapacity) {
    	return new HashSet<E>(initialCapacity);
    }
    
    public static <E> HashSet<E> HashSet(HashSet<E> s) {
    	return new HashSet<E>(s);
    }
    
    public static <E> HashSet<E> HashSet(int initialCapacity, int loadFactor) {	
    	return new HashSet<E>(initialCapacity, loadFactor);
    }
    
    // HashTable
    public static <K,V> Hashtable<K,V> Hashtable() {
	   	return new Hashtable<K,V>();
	}

    public static <K,V> Hashtable<K,V> Hashtable(int initialCapacity) {
    	return new Hashtable<K, V>(initialCapacity);
    }
    
    public static <K,V> Hashtable<K,V> Hashtable(Hashtable<K,V> t) {
    	return new Hashtable<K, V>(t);
    }
    
    public static <K,V> Hashtable<K,V> Hashtable(int initialCapacity, int loadFactor) {	
    	return new Hashtable<K, V>(initialCapacity, loadFactor);
    }

    // TreeMap
    public static <K,V> TreeMap<K,V> TreeMap() {
		return new TreeMap<K, V>();
	}
    
    public static <K,V> TreeMap<K,V> TreeMap(Comparator<? super K> comparator) {
    	return new TreeMap<K, V>(comparator);
	}
    
    public static <K,V> TreeMap<K,V> TreeMap(Map<? extends K, ? extends V> m) {
    	return new TreeMap<K, V>(m);
    }
    
    public static <K,V> TreeMap<K,V> TreeMap(SortedMap<K, ? extends V> m) {
    	return new TreeMap<K, V>(m);
    }
    
    // ArrayList
    public static <E> ArrayList<E> ArrayList() {
    	return new ArrayList<E>();
    }
    
    public static <E> ArrayList<E> ArrayList(Collection<? extends E> c) {
    	return new ArrayList<E>(c);
    }
    
    public static <E> ArrayList<E> ArrayList(int initialCapacity) {
    	return new ArrayList<E>(initialCapacity);
    }
    
    // ArrayDeque
    public static <E> ArrayDeque<E> ArrayDeque() {
    	return new ArrayDeque<E>();
	}
    
    public static <E> ArrayDeque<E> ArrayDeque(Collection<? extends E> c) {
    	return new ArrayDeque<E>(c);
	}
    
    public static <E> ArrayDeque<E> ArrayDeque(int numElements) {
    	return new ArrayDeque<E>(numElements);
    }
    
    // TODO linkedlist
}

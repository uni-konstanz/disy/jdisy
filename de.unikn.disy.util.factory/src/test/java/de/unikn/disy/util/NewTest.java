package de.unikn.disy.util;

import static org.junit.Assert.assertTrue;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Test;

public class NewTest {
		
	@Test
	public final void testHashMap() {
		HashMap<Integer, String> m = New.HashMap();
		assertTrue(m.equals(new HashMap<Integer, String>()));
	}

	@Test
	public final void testHashMapInt() {
		HashMap<Integer, String> m = New.HashMap(10);
		assertTrue(m.equals(new HashMap<Integer, String>(10)));
	}

	@Test
	public final void testHashMapHashMapOfKV() {
		HashMap<Integer, String> h = New.HashMap();
		HashMap<Integer, String> m = New.HashMap(h);
		assertTrue(m.equals(h));
	}

	@Test
	public final void testHashMapIntInt() {
		HashMap<Integer, String> m = New.HashMap(10,1);
		assertTrue(m.equals(new HashMap<Integer, String>(10,1)));
	}

	@Test
	public final void testHashSet() {
		HashSet<String> s = New.HashSet();
		assertTrue(s.equals(new HashSet<String>()));
	}

	@Test
	public final void testHashSetInt() {
		HashSet<String> s = New.HashSet(10);
		assertTrue(s.equals(new HashSet<String>(10)));
	}

	@Test
	public final void testHashSetHashSetOfE() {
		HashSet<String> z = new HashSet<String>();
		HashSet<String> s = New.HashSet(z);
		assertTrue(s.equals(z));
	}

	@Test
	public final void testHashSetIntInt() {
		HashSet<String> s = New.HashSet(10, 1);
		assertTrue(s.equals(new HashSet<String>(10,1)));
	}

	@Test
	public final void testHashtable() {
		Hashtable<Integer, String> h = New.Hashtable();
		assertTrue(h.equals(new Hashtable<Integer, String>()));
	}

	@Test
	public final void testHashtableInt() {
		Hashtable<Integer, String> h = New.Hashtable(10);
		assertTrue(h.equals(new Hashtable<Integer, String>(10)));
	}

	@Test
	public final void testHashtableHashtableOfKV() {
		Hashtable<Integer, String> k = new Hashtable<Integer, String>();
		Hashtable<Integer, String> h = New.Hashtable();
		assertTrue(h.equals(k));
	}

	@Test
	public final void testHashtableIntInt() {
		Hashtable<Integer, String> h = New.Hashtable(10,1);
		assertTrue(h.equals(new Hashtable<Integer, String>(10,1)));
	}

	@Test
	public final void testArraylist() {
		ArrayList<Integer> l = New.ArrayList();
		assertTrue(l.equals(new ArrayList<Integer>()));
	}

	@Test
	public final void testArrayListCollectionOfQextendsE() {
		ArrayList<Integer> a = new ArrayList<Integer>();
		ArrayList<Integer> l = New.ArrayList(a);
		assertTrue(l.equals(a));
	}

	@Test
	public final void testArrayListInt() {
		ArrayList<Integer> l = New.ArrayList(10);
		assertTrue(l.equals(new ArrayList<Integer>(10)));
	}

	@Test
	public final void testArrayDeque() {
		ArrayDeque<Integer> d = New.ArrayDeque();
		// equals not implemented by ArrayDeque, why?
		// assertTrue(d.equals(new ArrayDeque<Integer>()));
		assertTrue(d.add(1));
	}

	@Test
	public final void testArrayDequeCollectionOfQextendsE() {
		ArrayDeque<Integer> a = New.ArrayDeque();
		a.add(1);
		ArrayDeque<Integer> d = New.ArrayDeque(a);
		assertTrue(d.contains(1));
	}

	@Test
	public final void testArrayDequeInt() {
		ArrayDeque<Integer> d = New.ArrayDeque(10);
		d.add(1);
		assertTrue(d.size() == 1 && d.contains(1));
	}

	@Test
	public final void testTreeMap() {
		TreeMap<Integer,String> m = New.TreeMap();
		assertTrue(m.equals(new TreeMap<Integer,String>()));
	}

	@Test
	public final void testTreeMapComparatorOfQsuperK() {
		Comparator<Integer> comparator = new Comparator<Integer>() {
			public int compare(Integer o1, Integer o2) {
				return 0;
			}
		};
		TreeMap<Integer,String> m = New.TreeMap(comparator);
		assertTrue(m.equals(new TreeMap<Integer,String>(comparator)));
	}

	@Test
	public final void testTreeMapMapOfQextendsKQextendsV() {
		TreeMap<Integer, String> t = new TreeMap<Integer, String>();
		TreeMap<Integer,String> m = New.TreeMap(t);
		assertTrue(m.equals(t));
	}

	@Test
	public final void testTreeMapSortedMapOfKQextendsV() {
		SortedMap<Integer,String> s = new TreeMap<Integer, String>();
		TreeMap<Integer,String> t = new TreeMap<Integer, String>(s);
		TreeMap<Integer, String> m = New.TreeMap(s);
		assertTrue(m.equals(t));
	}

}

/*
	Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de
	
	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
 */
package de.unikn.disy.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;

/**
 * Provides methods to convert between data types.
 * 
 * @author zink
 */
public final class Convert {
    /**
     * Serializes an Object to byte[].
     * The whole Object is serialized. This is not the same as using
     * byte buffers.
     * 
     * @param obj the object to convert
     * @return the object as byte[]
     * @throws IOException 
     */
    public static byte[] objectToBytes (Object obj) throws IOException {
        ByteArrayOutputStream bstream = new ByteArrayOutputStream();
        ObjectOutputStream ostream = new ObjectOutputStream(bstream);
        ostream.writeObject(obj);
        return bstream.toByteArray();
    }
    
    /**
     * Creates an Object from a byte[]
     * 
     * @param bytes the byte array to convert
     * @return the converted object
     * @throws ClassNotFoundException 
     * @throws IOException 
     */
    public static Object bytesToObject (byte[] bytes) 
    		throws IOException, ClassNotFoundException {
        Object obj = null;
        obj = new ObjectInputStream(
        		new ByteArrayInputStream(bytes)
        ).readObject();
        return obj;
    }
    
    /**
     * Converts a Short to byte[]
     * 
     * @param val value to convert
     * @return the byte[] representation
     */
    public static byte[] shortToBytes (Short val) {        
        ByteBuffer buffer = ByteBuffer.allocate(2);
        buffer.putShort(val);
        byte[] bytes = buffer.array();
        buffer.clear();
        return bytes;
    }
    
    /**
     * Converts an Integer to byte[]
     * 
     * @param val value to convert
     * @return the byte[] representation
     */
    public static byte[] intToBytes (Integer val) {
        ByteBuffer buffer = ByteBuffer.allocate(4);
        buffer.putInt(val);
        byte[] bytes = buffer.array();
        buffer.clear();
        return bytes;
    }
    
    /**
     * Converts a Long to byte[]
     * 
     * @param val value to convert
     * @return the byte[] representation
     */
    public static byte[] longToBytes (Long val) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putLong(val);
        byte[] bytes = buffer.array();
        buffer.clear();
        return bytes;
    }
    
    /**
     * Converts a Float to byte[]
     * 
     * @param val value to convert
     * @return the byte[] representation
     */
    public static byte[] floatToBytes (Float val) {
        ByteBuffer buffer = ByteBuffer.allocate(4);
        buffer.putFloat(val);
        byte[] bytes = buffer.array();
        buffer.clear();
        return bytes;
    }
    
    /**
     * Converts a Double to byte[]
     * 
     * @param val value to convert
     * @return the byte[] representation
     */
    public static byte[] doubleToBytes (Double val) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putDouble(val);
        byte[] bytes = buffer.array();
        buffer.clear();
        return bytes;
    }
}

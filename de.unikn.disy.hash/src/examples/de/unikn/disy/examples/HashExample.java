/*
	Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de
	
	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
*/
package de.unikn.disy.examples;

import de.unikn.disy.hash.APHash;
import de.unikn.disy.hash.BKDRHash;
import de.unikn.disy.hash.BPHash;
import de.unikn.disy.hash.BUZHash;
import de.unikn.disy.hash.CBUHash;
import de.unikn.disy.hash.CRC32;
import de.unikn.disy.hash.DEKHash;
import de.unikn.disy.hash.DJBHash;
import de.unikn.disy.hash.ELFHash;
import de.unikn.disy.hash.FNVHash;
import de.unikn.disy.hash.JSHash;
import de.unikn.disy.hash.JavaHash;
import de.unikn.disy.hash.OATHash;
import de.unikn.disy.hash.PJWHash;
import de.unikn.disy.hash.PKPHash;
import de.unikn.disy.hash.RSHash;
import de.unikn.disy.hash.SDBMHash;

/**
 * Example usage of the jdisy_hash library
 * @author zink
 */
public class HashExample {
	/*
		this method is taken from apache StringUtils.
	 */
	public static String join(Object[] array, char separator, int startIndex, int endIndex) {
		if (array == null) {
			return null;
		}
		int bufSize = (endIndex - startIndex);
		if (bufSize <= 0) {
			return "";
		}
		bufSize *= ((array[startIndex] == null ? 16 : array[startIndex].toString().length()) + 1);
		StringBuilder buf = new StringBuilder(bufSize);
		
		for (int i = startIndex; i < endIndex; i++) {
			if (i > startIndex) {
				buf.append(separator);
			}
			if (array[i] != null) {
				buf.append(array[i]);
			}
		}
		return buf.toString();
	}
	
	public static void main(String[] args) {
		String key = args.length == 0 ? "Hello World!" : join(args,' ',0,args.length);
		byte[] bytes = key.getBytes();
		for (byte b : bytes) System.out.printf("%x ", b);
		System.out.println(" : " + key);
	            
		System.out.printf("APHash(), 0x%xL\n", new APHash().hash(bytes));
		System.out.printf("BKDRHash(), 0x%xL\n", new BKDRHash().hash(bytes));
		System.out.printf("BPHash(), 0x%xL\n", new BPHash().hash(bytes));
		System.out.printf("BUZHash(), 0x%xL\n", new BUZHash().hash(bytes));
		System.out.printf("CBUHash(), 0x%xL\n", new CBUHash().hash(bytes));
		System.out.printf("CRC32(), 0x%xL\n", new CRC32().hash(bytes));
		System.out.printf("DEKHash(), 0x%xL\n", new DEKHash().hash(bytes));
		System.out.printf("DJBHash(), 0x%xL\n", new DJBHash().hash(bytes));
		System.out.printf("ELFHash(), 0x%xL\n", new ELFHash().hash(bytes));
		System.out.printf("FNVHash(), 0x%xL\n", new FNVHash().hash(bytes));
		System.out.printf("JSHash(), 0x%xL\n", new JSHash().hash(bytes));
		System.out.printf("JavaHash(), 0x%xL\n", new JavaHash().hash(bytes));
		System.out.printf("PJWHash(), 0x%xL\n", new PJWHash().hash(bytes));
		System.out.printf("PKPHash(), 0x%xL\n", new PKPHash(1,8).hash(bytes));
		System.out.printf("RSHash(), 0x%xL\n", new RSHash().hash(bytes));
		System.out.printf("SDBMHash(), 0x%xL\n", new SDBMHash().hash(bytes));
		System.out.printf("JenkinsHash(), 0x%xL\n", new OATHash().hash(bytes));
	}
}

/*
	Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
 */
package de.unikn.disy.hash;

import de.unikn.disy.hash.HashFunction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author zink
 */
abstract public class HashFunctionTest {
    
    protected HashFunction func = null;
    protected final static String key = "Hello World!";
    protected final byte[] bytes = key.getBytes();
    protected long expResult = 0L;
    //protected long objResult = 0L;
    
    //public HashFunctionTest() {}
    
    /*public HashFunctionTest(HashFunction func, long expResult, long objResult) {
        this.func = func;
        this.expResult = expResult;
        this.objResult = objResult;
    }*/
    
    public HashFunctionTest(HashFunction func, long expResult) {
        this.func = func;
        this.expResult = expResult;
    }
    
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        this.func.reset();
    }

    // test hash functions
    
    /*@Test
    public void testHash_Object() {
        System.out.println(this.func.getClass().getName() + ".hash(Object)");
        long result = this.func.hash(key);
        assertEquals(this.objResult, result);
    }*/

    /**
     * Test of hash method, of class HashFunction.
     */
    @Test
    public void testHash_int() {
        System.out.println(this.func.getClass().getName() + ".hash(int)");
        this.func.update(this.bytes[0]);
        long expect = this.func.getValue();
        long result = this.func.hash(this.bytes[0]);
        assertEquals(expect, result);
    }
    
    /**
     * Test of hash method, of class HashFunction.
     */
    @Test
    public void testHash_byteArr() {
	// calls hash_4args
        System.out.println(this.func.getClass().getName() + ".hash(byte[])");
        long result = this.func.hash(this.bytes);
	assertEquals(this.expResult, result);
    }
    
    /**
     * Test of hash method, of class HashFunction.
     */
    @Test
    public void testHash_3args() {
	// calls hash_4args
        System.out.println(this.func.getClass().getName() + ".hash(byte[],int,int)");
        int off = 0;
        int len = this.bytes.length;
        long result = this.func.hash(this.bytes, off, len);
        assertEquals(this.expResult, result);
    }
    
    // test update functions
    
    /**
     * Test of update method, of class HashFunction.
     */
    /*@Test
    public void testUpdate_Object() {
	// calls update_byte[]
        System.out.println(this.func.getClass().getName() + ".update(Object)");
        this.func.update(this.key);
	long result = this.func.getValue();
	assertEquals(this.objResult, result);
    }*/
    
    /**
     * Test of update method, of class HashFunction.
     */
    /*@Test
    public void testUpdate_int() {
	// calls hash_4args
        System.out.println(this.func.getClass().getName() + ".update(int)");
        for (byte b : this.bytes) {
            this.func.update(b);
        }
        long result = this.func.getValue();
        assertEquals(this.expResult, result);
    }*/
    
    @Test
    public void testUpdate_int() {
	// calls hash_4args
        System.out.println(this.func.getClass().getName() + ".update(int)");
        this.func.update(this.bytes[0]);
        long expect = this.func.hash(this.bytes[0]);
        long result = this.func.getValue();
        assertEquals(expect, result);
    }
    
    /**
     * Test of update method, of class HashFunction.
     */
    @Test
    public void testUpdate_byteArr() {
	// calls hash_4args
	System.out.println(this.func.getClass().getName() + ".update(byte[])");
	this.func.update(this.bytes);
        long result = this.func.getValue();
	assertEquals(this.expResult, result);
    }

    /**
     * Test of update method, of class HashFunction.
     */
    /*@Test
    public void testUpdate_3args() {
	// calls hash_4args
        System.out.println(this.func.getClass().getName() + ".update(byte[],int,int)");
        this.func.update(this.bytes, 0, 6);
        this.func.update(this.bytes, 6, 6);
        long result = this.func.getValue();
        assertEquals(this.expResult, result);
    }*/
    
    @Test
    public void testUpdate_3args() {
	// calls hash_4args
        System.out.println(this.func.getClass().getName() + ".update(byte[],int,int)");
        this.func.update(this.bytes, 3, 7);
        long expect = this.func.hash(this.bytes, 3, 7);
        long result = this.func.getValue();
        assertEquals(expect, result);
    }

    // test getter
    
    //public long getValue();
    //public void reset();
    //public int bytecount();
}

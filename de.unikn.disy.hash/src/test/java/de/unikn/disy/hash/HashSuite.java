/*
	Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
 */
package de.unikn.disy.hash;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author zink
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    de.unikn.disy.hash.APHashTest.class, de.unikn.disy.hash.BKDRHashTest.class, de.unikn.disy.hash.BPHashTest.class,
    de.unikn.disy.hash.BUZHashTest.class, de.unikn.disy.hash.DEKHashTest.class, de.unikn.disy.hash.DJBHashTest.class,
    de.unikn.disy.hash.ELFHashTest.class, de.unikn.disy.hash.FNVHashTest.class, de.unikn.disy.hash.JSHashTest.class,
    de.unikn.disy.hash.JavaHashTest.class, de.unikn.disy.hash.PJWHashTest.class, de.unikn.disy.hash.PKPHashTest.class,
    de.unikn.disy.hash.RSHashTest.class, de.unikn.disy.hash.SDBMHashTest.class, de.unikn.disy.hash.OATHashTest.class,
    de.unikn.disy.hash.CRC32Test.class
})
public class HashSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}

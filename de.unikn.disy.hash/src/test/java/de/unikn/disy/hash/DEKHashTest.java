/*
	Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
 */
package de.unikn.disy.hash;

import de.unikn.disy.hash.DEKHash;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author zink
 */
public class DEKHashTest extends HashFunctionTest {
    
    public DEKHashTest() {
        //super(new DEKHash(), 0xe599ef706958d0b1L, 0x285afb7864d80db1L);
    	super(new DEKHash(), 0xe599ef706958d0b1L);
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Override
    @Test
    public void testUpdate_int() {
	// calls hash_4args
        System.out.println(this.func.getClass().getName() + ".update(int)");
        long expect = this.func.hash(this.bytes[0]);
        this.func.update(this.bytes[0]);
        long result = this.func.getValue();
        assertEquals(expect, result);
    }
    
    @Override
    @Test
    public void testUpdate_3args() {
	// calls hash_4args
        System.out.println(this.func.getClass().getName() + ".update(byte[],int,int)");
        long expect = this.func.hash(this.bytes, 0, 6);
        this.func.update(this.bytes, 0, 6);
        long result = this.func.getValue();
        assertEquals(expect, result);
    }
}

/*
	Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
 */
package de.unikn.disy.hash;

import java.util.Random;

/**
 * Originally proposed by Peter K. Pearson [1]
 * Intended for byte strings to produce byte hash values. Wider hashes require
 * one additional table (implemented as 2d array) per additional output byte.
 * Thus it is an expensive hash both in terms of memory as well as computation.
 * 
 * [1] Pearson, Peter K.:
 *      Fast Hashing of Variable Length Text Strings.
 *      Commun. ACM 33,6 (Jun 1990), p677.
 * 
 * @author zink
 */
public final class PKPHash extends HashFunction {
    private final byte[][] table;
    private final int length;
    
    public PKPHash() {
        this(System.nanoTime(),1);
    }
    
    public PKPHash(long seed, int num) {
        length = Math.abs(num) > 8 ? 8 : Math.abs(num);
        Random r = new Random(seed);
        table = new byte[length][0xFF];
        for (int i=0; i<length; i++) {
            r.nextBytes(table[i]);
        }
    }
    
    @Override
    public long hash(Long init, final byte[] b, int off, int len) {
        long hash = init;
        while (--len >= 0) {
            for (int j=0; j<length; j++) {
                hash ^= (table[j][(int)(b[off]&0xff)]&0xff) << (j*8);
            }
            off++;
        }
        return hash;
    }
}

/*
	Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
 */
package de.unikn.disy.hash;

/**
 * Proposed by Peter J. Weinberger. 
 * 
 * @author zink
 */
public final class PJWHash extends HashFunction {
    private final static long BitsInUnsignedInt = (long)(4 * 8);
    private final static long ThreeQuarters     = (long)((BitsInUnsignedInt  * 3) / 4);
    private final static long OneEighth         = (long)(BitsInUnsignedInt / 8);
    private final static long HighBits          = (long)(0xFFFFFFFF) << (BitsInUnsignedInt - OneEighth);
    
    @Override
    public long hash(Long init, final byte[] b, int off, int len) {
        long hash = init;
        long test = 0L;
        while (--len >= 0) {
            hash = (hash << OneEighth) + b[off++];
            if ((test = hash & HighBits) != 0)
                hash = ((hash ^ (test >> ThreeQuarters)) & (~HighBits));
        }
        return hash;
    }
}

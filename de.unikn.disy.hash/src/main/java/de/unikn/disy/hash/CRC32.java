/*
	Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de
	
	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
 */
package de.unikn.disy.hash;

/**
 * Implements CRC32 that is equal to java.util.zip.CRC32.
 * 
 * @author zink
 */
public class CRC32 extends HashFunction {
    
	private static final int CRC_32IEEE = 0xEDB88320;
    // these are well known constants for crc32 and included here for
	// completeness
	@SuppressWarnings("unused")
	private static final int CRC_32C = 0x82F63B78;
    @SuppressWarnings("unused")
    private static final int CRC_32K = 0xEB31D82E;
    @SuppressWarnings("unused")
    private static final int CRC_32Q = 0xD5828281;
    
    
    private static int[] table = new int[0xFF];
    
    public CRC32() {
        this(CRC_32IEEE);
    }
    
    public CRC32(int poly) {
        buildTable(poly);
    }

    private static void buildTable(int poly) {
        for (int i = 0; i < 0xFF; i++) {
            int c = i;
            for (int j = 8;  --j >= 0; ) {
                c = ((c & 1) != 0) ? poly ^ (c >>> 1) : c >>> 1;
            }
            table[i] = c;
        }
    }
    
    @Override
    public long hash(Long init, byte[] b, int off, int len) {
        int hash = ~(init.intValue());
        while(--len >= 0) {
            hash = table[(hash ^ b[off++]) & 0xff] ^ (hash >>> 8);
        }
        return ~hash;
    }
    
    
}

/*
	Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de
	
	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
 */
package de.unikn.disy.hash;

/**
 * Implements a hash function proposed by Brian Kernighan and Dennis Ritchie
 * in their book "The C Programming Language", second edition, p. 128.
 * 
 * unsigned hash(char *s) {
 *      unsigned hashval;
 *      for (hashval = 0; *s != '\0'; s++) hashval = *s + 31 * hashval;
 *      return hashval;
 * }
 * 
 * @author zink
 */
public final class BKDRHash extends HashFunction {
    private final static long SEED = 31L; //313L, 3131L ...;
   
    @Override
    public long hash(Long init, final byte[] b, int off, int len) {
        long hash = init;
        while (--len >= 0) {
            hash = (hash * SEED) + b[off++];
        }
        return hash;
    }
}

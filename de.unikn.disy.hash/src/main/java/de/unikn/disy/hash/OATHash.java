/*
	Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
 */
package de.unikn.disy.hash;

/**
 * Implements a hash function proposed by Bob Jenkins in [1]. Aka
 * One-At-a-Time Hash.
 * 
 * "This is similar to [a] rotating hash, ... but it mixes the internal state.
 * Analysis suggests there are no funnels"[1]. The OATHash
 * is also suitable for computing checksums (see [2]), however, it
 * will produce different values when used consecutively, since some 
 * arithmetic is done outside the loop.
 * 
 * [1] http://www.burtleburtle.net/bob/hash/doobs.html
 * [2] http://en.wikipedia.org/wiki/Jenkins_hash_function
 * 
 * @author zink
 */
public class OATHash extends HashFunction {
    @Override
    public long hash(Long init, final byte[] b, int off, int len) {
        long hash = init;
        while (--len >= 0) {
            hash += b[off++];
            hash += (hash << 10);
            hash ^= (hash >> 6);
        }
        hash += (hash << 3);
        hash ^= (hash >> 11);
        hash += (hash << 15);
        return hash;
    }
}
